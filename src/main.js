// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'expose-loader?jQuery!jquery' // eslint-disable-line
import 'expose-loader?$!jquery' // eslint-disable-line
import 'vue-material/dist/vue-material.min.css'
import 'leaflet/dist/leaflet.css';
import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import VueMaterial from 'vue-material';

import router from './Routes';
import App from './App';
import axios from 'axios';
import VueAxios from 'vue-axios';

import api from './utilities/api'

Vue.use(BootstrapVue);
Vue.use(VueAxios, axios);
Vue.use(VueMaterial);
Vue.mixin(api);
/* eslint-disable */

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App),
});
